package com.movix.katas.one;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.movix.katas.one.exception.NoValidMsisdnException;

public class KataOneTest {

	private static final String MESSAGE_MSISDN_SEPARATOR = " ";

	private static final String ANY_COMMAND = "any";

	private static final List<String> SUBSCRIPTION_COMMANDS = Arrays.asList("juegos", "horoscopo");
	private static final String WELCOME_MESSAGE_PREFIX = "Bienvenido. Ya te encuentras suscrito a ";
	private static final String UNSUBSCRIBED_MESSAGE_PREFIX = "Te has desuscrito de ";

	private static final String UNKNOWN_COMMAND_MESSAGE = "Comando desconocido. Envie \nayuda\n para ver los comandos disponibles";
	private static final String HELP_MESSAGE = "Envia \"echo\" para probar el servicio. Envia \"juegos\" para suscribirte a Juegos.";
	private static final String GIFT_MESSAGE = "Has regalado un paquete de datos a tu amig@";

	private static final String MSISDN = "56000000001";
	private static final String COMMAND_MSISDN = "56000000002";
	private static final String COMMAND_LOCAL_MSISDN = "000000002";
	private static final String COMMAND_LOOKS_LIKE_A_LOCAL_MSISDN = "A00000002";

	@Test
	public void processCommand_WhenNoCommand_ReturnNull() {
		assertThat(_processCommandNoException(MSISDN, null), is(nullValue()));
		assertThat(_processCommandNoException(MSISDN, ""), is(nullValue()));
	}

	@Test
	public void processCommand_WhenEchoCommnd_ReturnEcho() {
		assertThat(_processCommandNoException(MSISDN, "echo"), is("echo"));
	}

	@Test
	public void processCommand_WhenEchoWithAnyCaseCommand_ReturnEchoWithAnyCase() {
		assertThat(_processCommandNoException(MSISDN, "ECHO"), is("ECHO"));
		assertThat(_processCommandNoException(MSISDN, "ecHO"), is("ecHO"));
	}

	@Test
	public void processCommand_WhenIsASubscriptionCommand_ReturnTheSubscriptionWelcomeMessage() {
		SUBSCRIPTION_COMMANDS.forEach(command -> assertThat(_processCommandNoException(MSISDN, command),
				is(WELCOME_MESSAGE_PREFIX + command.toUpperCase())));
	}

	@Test
	public void processCommand_WhenHelpCommand_ReturnHelpMessage() {
		assertThat(_processCommandNoException(MSISDN, "ayuda"), is(HELP_MESSAGE));
	}

	@Test
	public void processCommand_WhenMsisdnCommand_ReturnGiftMessagePlusSpacePlusLocalMsisdn()
			throws NoValidMsisdnException {
		KataOne t = new KataOne();
		assertThat(t.processCommand(MSISDN, COMMAND_MSISDN), startsWith(GIFT_MESSAGE));
		assertThat(t.processCommand(MSISDN, COMMAND_MSISDN),
				is(GIFT_MESSAGE + MESSAGE_MSISDN_SEPARATOR + COMMAND_LOCAL_MSISDN));
	}

	@Test
	public void processCommand_WhenLocalMsisdnCommand_ReturnGiftMessagePlusSpacePlusLocalMsisdn()
			throws NoValidMsisdnException {
		KataOne t = new KataOne();
		assertThat(t.processCommand(MSISDN, COMMAND_LOCAL_MSISDN),
				is(GIFT_MESSAGE + MESSAGE_MSISDN_SEPARATOR + COMMAND_LOCAL_MSISDN));
	}

	@Test
	public void processCommand_WhenCommandLooksLikeLocalMsisdn_ReturnUnknownCommand() throws NoValidMsisdnException {
		KataOne t = new KataOne();
		assertThat(t.processCommand(MSISDN, COMMAND_LOOKS_LIKE_A_LOCAL_MSISDN), is(UNKNOWN_COMMAND_MESSAGE));
	}

	@Test
	public void processCommand_WhenExitSubscriptionCommand_ReturnUnsubscribedMessage() {
		SUBSCRIPTION_COMMANDS.forEach(command -> {
			String unsubscribedMessage = UNSUBSCRIBED_MESSAGE_PREFIX + command.toUpperCase();
			assertThat(_processCommandNoException(MSISDN, "salir " + command), is(unsubscribedMessage));
			assertThat(_processCommandNoException(MSISDN, "SALIR " + command), is(unsubscribedMessage));
			String mixedExitCommand = "SAlIR ";
			assertThat(_processCommandNoException(MSISDN, mixedExitCommand + command), is(unsubscribedMessage));
			assertThat(_processCommandNoException(MSISDN, mixedExitCommand + command.toUpperCase()),
					is(unsubscribedMessage));
			assertThat(
					_processCommandNoException(MSISDN,
							mixedExitCommand + command.charAt(0) + command.toUpperCase().substring(1)),
					is(unsubscribedMessage));
		});
	}

	@Test(expected = NoValidMsisdnException.class)
	public void processCommand_WhenNoMsisdn_ThrowNoValidMsisdn() throws NoValidMsisdnException {
		KataOne t = new KataOne();
		t.processCommand(null, "juegos");
	}

	@Test(expected = NoValidMsisdnException.class)
	public void processCommand_WhenInvalidMsisdnIsShorter_ThrowNoValidMsisdn() throws NoValidMsisdnException {
		KataOne t = new KataOne();
		t.processCommand("1234", ANY_COMMAND);
	}

	@Test(expected = NoValidMsisdnException.class)
	public void processCommand_WhenInvalidMsisdnIsLonger_ThrowNoValidMsisdn() throws NoValidMsisdnException {
		KataOne t = new KataOne();
		t.processCommand("12343333322222", ANY_COMMAND);
	}

	private String _processCommandNoException(String msisdn, String command) {
		try {
			KataOne t = new KataOne();
			return t.processCommand(msisdn, command);
		} catch (NoValidMsisdnException e) {
			// Not expected exception
		}
		return null;
	}

}
