¡Hola!

Si estás por acá, es porque has sido asignado para realizar una modificación a esta aplicación, así que vamos al grano.

## Requerimiento ##

* En la clase ***com.movix.katas.one.KataOne*** es necesario procesar el comando "**SALIR <SUSCRIPCION>**" (que podría venir en mayúsculas, minúsculas o una combinación de estas).
* El mensaje de respuesta debe ser "**Te has desuscrito de <SUSCRIPCION>**" (el valor de SUSCRIPCION debe ir en mayúsculas).
* El valor de SUSCRIPCION puede ser:
    * juegos
    * horoscopo

### Ejemplo ###

* Comando: "salir Juegos"
* Mensaje de respuesta: "Te has desuscrito de JUEGOS"

### Importante ###

* La aplicación cuenta con pruebas unitarias que dan cobertura al 100% del código.
* La prueba unitaria para este requerimiento ya se encuentra programada, por lo que es la única que no está pasando.

## Consideraciones ##

* **Has un fork. Haz tu proyecto privado. Agrega a @vvalencia-movix como usuario de tu proyecto, con acceso de lectura**
* Necesitas tener instalado JRE ó JDK 1.8

## Evaluación ##

* Se evaluará la ejecución correcta del build, la calidad del código y las buenas prácticas de desarrollo efectivamente aplicadas.

* **El código es tuyo**. Si tienes una mejor idea de como implementar la lógica, agregar alguna lib que ahorre trabajo, crear otras clases, aplicar patrones/principios de diseño de software... ¡adelante!

## Entrega ##

* Cuando hayas terminado tu implementación/mejoras, crea un pull request sobre tu proyecto y agrega a @vvalencia-movix como reviewer (si tu nombre de usuario no se parece a tu nombre real, déjanos tu nombre en el asunto del pull request). Importante: Si no haces el pull request sobre tu proyecto, tu pull request quedará público.
* ¡Eso es todo!

## Revisión ##

* Si el pull request está de acuerdo a las indicaciones, se marcará como Aprobado
* Si el pull request está intentando integrarse al proyecto original, se marcará como Rechazado (para que quede oculto de la vista pública)
